/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

/**
 *
 * @author Carlos Omar
 */
public class Numero {
    
    private int numero;

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public int sumaDigitos(){
        int nroAux = numero;
        int suma = 0;
        int residuo;
        while (nroAux>0) {
            residuo = nroAux%10;
            suma = suma + residuo;
            nroAux = nroAux/10;
        }
        
        return suma;
    }
    
    public static void main(String[] args) {
        Numero n = new Numero();
        n.setNumero(1234);
        System.out.println("suma: " + n.sumaDigitos());
    }
    
}
